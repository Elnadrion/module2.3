//
//  skillbox_3App.swift
//  skillbox_3
//
//  Created by Elnadrion on 12.09.2021.
//

import SwiftUI

@main
struct skillbox_3App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
