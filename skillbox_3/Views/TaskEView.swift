//
//  TaskEView.swift
//  TaskEView
//
//  Created by Elnadrion on 12.09.2021.
//

import SwiftUI

struct TaskEView: View {
    @StateObject private var viewModel = TaskEViewModel()
    
    var body: some View {
        VStack {
            Text(viewModel.labelText)
            
            HStack {
                Button(action: {
                    viewModel.button1pressed = true
                }, label: {
                    Text("Button 1")
                }).padding()
                Button(action: {
                    viewModel.button2pressed = true
                }, label: {
                    Text("Button 2")
                }).padding()
            }
        }
    }
}

struct TaskEView_Previews: PreviewProvider {
    static var previews: some View {
        TaskEView()
    }
}
