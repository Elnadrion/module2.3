//
//  TaskBView.swift
//  TaskBView
//
//  Created by Elnadrion on 12.09.2021.
//

import SwiftUI

struct TaskBView: View {
    @State var searchString: String = ""
    @StateObject private var viewModel = TaskBViewModel()
    
    var body: some View {
        NavigationView {
            VStack {
                Form {
                    Section(header: Text("Search")) {
                        TextField("", text: $searchString)
                            .autocapitalization(.none)
                            .onChange(of: searchString) {
                                viewModel.passthrough.send($0)
                            }
                    }
                }
            }
        }.navigationTitle("Task B")
    }
}

struct TaskBView_Previews: PreviewProvider {
    static var previews: some View {
        TaskBView()
    }
}
