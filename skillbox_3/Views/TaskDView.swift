//
//  TaskDView.swift
//  TaskDView
//
//  Created by Elnadrion on 12.09.2021.
//

import SwiftUI

struct TaskDView: View {
    @StateObject var model: TaskDViewModel = TaskDViewModel()
    
    var body: some View {
        VStack {
            Text(String(model.counter))
                .padding(5)
            Button("Counter") {
                model.passthrough.send()
            }
        }
    }
}

struct TaskDView_Previews: PreviewProvider {
    static var previews: some View {
        TaskDView()
    }
}
