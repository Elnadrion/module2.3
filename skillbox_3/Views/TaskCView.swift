//
//  TaskCView.swift
//  TaskCView
//
//  Created by Elnadrion on 12.09.2021.
//

import SwiftUI

struct TaskCView: View {
    @StateObject var viewModel = TaskCViewModel()
    
    var body: some View {
        NavigationView {
            VStack {
                Form {
                    TextField("Search query", text: $viewModel.searchQuery)
                        .autocapitalization(.none)
                        .padding(10)
                    Spacer()
                    List(viewModel.persons) { person in
                        Text(person.name)
                    }
                }
             
            }
        }
        .navigationTitle(Text("Task C"))
        .navigationBarItems(
            leading: Button(action: remove, label: { Text("Remove") }),
            trailing: Button(action: add, label: { Text("Add") })
        )
    }
    
    func remove() {
        viewModel.removeEvent.send()
    }
    
    func add() {
        viewModel.addEvent.send()
    }
}

struct TaskCView_Previews: PreviewProvider {
    static var previews: some View {
        TaskCView()
    }
}
