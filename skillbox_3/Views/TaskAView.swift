//
//  TaskAView.swift
//  TaskAView
//
//  Created by Elnadrion on 12.09.2021.
//

import SwiftUI

struct TaskAView: View {
    @StateObject private var formViewModel = TaskAViewModel()
    
    var body: some View {
        NavigationView {
            VStack {
                Form {
                    Section(header: Text("Login")) {
                        TextField("example@email.com", text: $formViewModel.login)
                            .autocapitalization(.none)
                    }
                    Section(header: Text("Password"), footer: Text(formViewModel.error).foregroundColor(.red)) {
                        SecureField("******", text: $formViewModel.password)
                            .autocapitalization(.none)
                    }
                }
                Button(action: {}) {
                    RoundedRectangle(cornerRadius: 10)
                        .frame(height: 60)
                        .overlay(Text("Continue").foregroundColor(.white))
                }.padding().disabled(!formViewModel.isValid)
            }
        }.navigationTitle("Sign up")
    }
}

struct TaskAView_Previews: PreviewProvider {
    static var previews: some View {
        TaskAView()
    }
}
