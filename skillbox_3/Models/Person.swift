//
//  Person.swift
//  Person
//
//  Created by Elnadrion on 12.09.2021.
//

import Foundation

struct Person: Identifiable {
    let id: Int
    let name: String
}
