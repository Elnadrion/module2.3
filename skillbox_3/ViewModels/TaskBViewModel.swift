//
//  TaskBViewModel.swift
//  TaskBViewModel
//
//  Created by Elnadrion on 12.09.2021.
//

import Combine
import SwiftUI

class TaskBViewModel: ObservableObject {
    
    @Published var counter: Int = 0
    
    var cancellables = Set<AnyCancellable>()
    public var passthrough = PassthroughSubject<String, Never>()
    
    init() {
        self.passthrough
            .debounce(for: 0.5, scheduler: RunLoop.main)
            .sink(receiveValue: {
                print("Отправка запроса для \($0)")
            }).store(in: &cancellables)
    }

}
