//
//  TaskAViewModel.swift
//  TaskAViewModel
//
//  Created by Elnadrion on 12.09.2021.
//

import SwiftUI
import Combine

class TaskAViewModel: ObservableObject {
    @Published var login = ""
    @Published var password = ""
    @Published var error = ""
    
    @Published var isValid = false
    
    private var cancellables = Set<AnyCancellable>()
    
    private var isLoginValidPublisher: AnyPublisher<Bool, Never> {
        $login
            .debounce(for: 0.8, scheduler: RunLoop.main)
            .removeDuplicates()
            .map { $0.count >= 3 && $0.isValidEmail() }
            .eraseToAnyPublisher()
    }
    
    private var isPasswordStrongEnoughPublisher: AnyPublisher<Bool, Never> {
        $password
            .debounce(for: 0.2, scheduler: RunLoop.main)
            .removeDuplicates()
            .map {
                $0.count >= 6
            }
            .eraseToAnyPublisher()
    }
    
    private var isFormValidPublisher: AnyPublisher<Bool, Never> {
        Publishers.CombineLatest(isLoginValidPublisher, isPasswordStrongEnoughPublisher)
            .map { $0 && $1 }
            .eraseToAnyPublisher()
    }
    
    init() {
        isFormValidPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.isValid, on: self)
            .store(in: &cancellables)
        
        isLoginValidPublisher
            .dropFirst()
            .receive(on: RunLoop.main)
            .map { isOK in
                return isOK ? "" : "Некорректная почта"
            }
            .assign(to: \.error, on: self)
            .store(in: &cancellables)
        
        isPasswordStrongEnoughPublisher
            .dropFirst()
            .receive(on: RunLoop.main)
            .map { isOk in
                return isOk ? "" : "Слишком короткий пароль"
            }
            .assign(to: \.error, on: self)
            .store(in: &cancellables)
    }
}
