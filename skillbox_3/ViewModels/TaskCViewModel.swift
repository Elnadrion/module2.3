//
//  TaskCViewModel.swift
//  TaskCViewModel
//
//  Created by Elnadrion on 12.09.2021.
//

import Foundation
import Combine

class TaskCViewModel: ObservableObject {

    var cancellables = Set<AnyCancellable>()
    public var addEvent = PassthroughSubject<Void, Never>()
    public var removeEvent = PassthroughSubject<Void, Never>()
    
    @Published var searchQuery: String = ""
    
    @Published var availableNames: [Person] = [
        Person(id: 0, name: "Amayah Hilton"),
        Person(id: 1, name: "Koby Stephens"),
        Person(id: 2, name: "Alison Barton"),
        Person(id: 3, name: "Alena Yates"),
        Person(id: 4, name: "Karishma Sanchez"),
        Person(id: 5, name: "Vinny Good"),
        Person(id: 6, name: "Rayhan Christie"),
        Person(id: 7, name: "Lola-Mae Davenport"),
        Person(id: 8, name: "Romany Conway"),
        Person(id: 9, name: "Dione Ventura"),
        Person(id: 10, name: "Herbert Munoz"),
        Person(id: 11, name: "Zaara Knapp"),
        Person(id: 12, name: "Samiya Harper"),
        Person(id: 13, name: "Christos Drew"),
        Person(id: 14, name: "Tarun Paul"),
        Person(id: 15, name: "Nyah Gallegos"),
        Person(id: 16, name: "Rojin Cobb"),
        Person(id: 17, name: "Ella-May Dupont"),
        Person(id: 18, name: "Andrew Mejia"),
        Person(id: 19, name: "Isobelle Noble"),
        Person(id: 20, name: "Ralphie Castaneda"),
    ]
    
    @Published var persons: [Person] = []
    
    private var searchPublisher: AnyPublisher<String, Never> {
        $searchQuery
            .debounce(for: 2.0, scheduler: RunLoop.main)
            .removeDuplicates()
            .eraseToAnyPublisher()
    }
    
    init() {
        self.persons = self.persons + self.availableNames
 
        self.addEvent
            .sink(receiveValue: { _ in
                if let randomPerson = self.persons.randomElement() {
                    self.persons.insert(randomPerson, at: 0)
                }
            }).store(in: &cancellables)
        
        self.removeEvent
            .sink(receiveValue: { _ in
                
                if self.persons.count > 0 {
                    self.persons.removeLast()
                }

            }).store(in: &cancellables)
        
        self.searchPublisher
            .dropFirst()
            .receive(on: RunLoop.main)
            .throttle(
                for: .seconds(2),
                   scheduler: RunLoop.main,
                   latest: true
            )
            .sink { value in
                if value.count > 0 {
                    self.persons = self.availableNames.filter {
                        $0.name.contains(value)
                    }
                } else {
                    self.persons = self.availableNames
                }
            }
            .store(in: &cancellables)
    }
}
