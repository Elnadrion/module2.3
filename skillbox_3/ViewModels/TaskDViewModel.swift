//
//  TaskDViewModel.swift
//  TaskDViewModel
//
//  Created by Elnadrion on 12.09.2021.
//

import Combine

class TaskDViewModel: ObservableObject {
    
    @Published var counter: Int = 0
    
    var cancellables = Set<AnyCancellable>()
    public var passthrough = PassthroughSubject<Void, Never>()
    
    init() {
        self.passthrough.sink(receiveValue: {
            self.counter = self.counter + 1
        }).store(in: &cancellables)
    }

}
