//
//  TaskEViewModel.swift
//  TaskEViewModel
//
//  Created by Elnadrion on 12.09.2021.
//

import SwiftUI
import Combine

class TaskEViewModel: ObservableObject {
    @Published var button1pressed: Bool = false
    @Published var button2pressed: Bool = false
    @Published var labelText: String = ""
    
    private var cancellables = Set<AnyCancellable>()
    
    private var isButton1Pressed: AnyPublisher<Bool, Never> {
        $button1pressed
            .dropFirst()
            .map { _ in true }
            .eraseToAnyPublisher()
    }
    
    private var isButton2Pressed: AnyPublisher<Bool, Never> {
        $button2pressed
            .dropFirst()
            .map { _ in true }
            .eraseToAnyPublisher()
    }
    
    private var isFormValidPublisher: AnyPublisher<String, Never> {
        Publishers.CombineLatest(isButton1Pressed, isButton2Pressed)
            .map { $0 && $1 ? "Ракета запущена" : ""}
            .eraseToAnyPublisher()
    }
    
    init() {
        isFormValidPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.labelText, on: self)
            .store(in: &cancellables)

    }
}
