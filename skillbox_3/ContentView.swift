//
//  ContentView.swift
//  skillbox_3
//
//  Created by Elnadrion on 12.09.2021.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            VStack {
                NavigationLink("A", destination: TaskAView())
                    .navigationBarTitle("Skillbox")
                NavigationLink("B", destination: TaskBView())
                NavigationLink("C", destination: TaskCView())
                NavigationLink("D", destination: TaskDView())
                NavigationLink("E", destination: TaskEView())
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
